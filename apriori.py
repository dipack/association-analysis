import collections
from itertools import combinations, chain
from argparse import ArgumentParser

def setup_argparser():
    parser = ArgumentParser()
    parser.add_argument("-f", "--file", type=str, help="Input file to run Apriori + Association Analysis on", default="./data/test_data.txt")
    parser.add_argument("-c", "--confidence", type=float, help="Min confidence to use", default=0.7)
    parser.add_argument("-s", "--support", type=float, help="Min support to use", default=0.5)
    return parser

class RuleTypes:
    RULE = "RULE"
    BODY = "BODY"
    HEAD = "HEAD"

class RuleCounts:
    ANY = -1
    NONE = 0

class TemplateOperations:
    ONEORONE = "1or1"
    ONEANDONE = "1and1"
    ONEORTWO = "1or2"
    ONEANDTWO = "1and2"
    TWOORTWO = "2or2"
    TWOANDTWO = "2and2"

class Rule:
    def __init__(self, head, body, conf=0.0):
        self.head = head
        self.body = body
        self.confidence = conf

    def __repr__(self):
        return "<AssociationRule head={0} => body={1}, confidence={2}>".format(self.head, self.body, self.confidence)

def load_data(filename):
    lines = []
    with open(filename, "r") as f:
        lines = [l.strip().split("\t") for l in f.readlines() if l.strip()]
    return lines

def format_data(data):
    all_genes = []
    all_diseases = []
    for row in data:
        if not row:
            continue
        disease_name = row[-1]
        genes = ["G{0}_{1}".format(idx + 1, g) for idx, g in enumerate(row[:-1])]
        genes.append(disease_name)
        all_genes.append(genes); all_diseases.append(disease_name)
    return all_genes, all_diseases

def subsets_of(arr):
    return chain(*[combinations(arr, idx + 1) for idx, _ in enumerate(arr)])

def join_set(item_set, length):
    """
    Join a set with itself, and return itemsets of the specified length
    """
    return set([item.union(iitem) for item in item_set for iitem in item_set if len(item.union(iitem)) == length])

def get_support(item, transactions, frequent_set):
    return float(frequent_set[item]) / len(transactions)

def get_items_with_support(item_set, transactions, support, frequent_set):
    items = set()
    local_set = collections.defaultdict(int)

    for item in item_set:
        for transaction in transactions:
            if item.issubset(transaction):
                frequent_set[item] += 1
                local_set[item] += 1

    for item, count in local_set.items():
        item_support = float(count) / len(transactions)
        if item_support >= support:
            items.add(item)

    return items

def get_item_set_transaction_list(data):
    transactions = []
    item_set = set()
    for row in data:
        transaction = frozenset(row)
        transactions.append(transaction)
        for item in transaction:
            item_set.add(frozenset( [item] ))
    return item_set, transactions

def compute_apriori(data, min_support=0.5, min_confidence=0.7):
    item_set, transactions = get_item_set_transaction_list(data)

    frequent_set = collections.defaultdict(int)
    global_set = {}

    association_rules = {}

    one_conf_set = get_items_with_support(item_set, transactions, min_support, frequent_set)

    current_set = one_conf_set
    idx = 2
    while current_set != set([]):
        global_set[idx - 1] = current_set
        current_set = join_set(current_set, idx)
        current_conf_set = get_items_with_support(current_set, transactions, min_support, frequent_set)
        current_set = current_conf_set
        idx += 1

    relevant_items = []
    for k, v in global_set.items():
        relevant_items.extend([ (tuple(item), get_support(item, transactions, frequent_set)) for item in v ])

    relevant_rules = []
    for k, v in list(global_set.items())[1:]:
        for item in v:
            subsets = [frozenset(ss) for ss in subsets_of(item)]
            for el in subsets:
                rem = item.difference(el)
                if len(rem):
                    conf = get_support(item, transactions, frequent_set) / get_support(el, transactions, frequent_set)
                    if conf >= min_confidence:
                        relevant_rules.append( Rule(tuple(el), tuple(rem), conf) )
    # Sort items based on support, and rules based on confidence
    return sorted(relevant_items, key=lambda item: item[1]), sorted(relevant_rules, key=lambda rule: rule.confidence)

def lookup(rules, rule_type, rule_count, rule_items):
    relevant_rules = set()
    if len(rules) == 0 or len(rule_items) == 0:
        return list(relevant_rules), len(relevant_rules)
    for rule in rules:
        head, body = rule.head, rule.body
        r = None
        if rule_type == RuleTypes.HEAD:
            r = list(head)
        elif rule_type == RuleTypes.BODY:
            r = list(body)
        else:
            r = list(head) + list(body)
        if rule_count == RuleCounts.ANY:
            if any([True if item in r else False for item in rule_items]):
                relevant_rules.add(rule)
        elif rule_count == RuleCounts.NONE:
            if all([True if item not in r else False for item in rule_items]):
                relevant_rules.add(rule)
        elif isinstance(rule_count, (int, float)):
            c = int(rule_count)
            found_sum = 0
            for item in rule_items:
                found_sum += r.count(item)
            if found_sum == c:
                relevant_rules.add(rule)
    return list(relevant_rules), len(relevant_rules)

def lookup_size(rules, rule_type, rule_count):
    relevant_rules = set()
    if len(rules) == 0:
        return list(relevant_rules), len(relevant_rules)
    for rule in rules:
        head, body = rule.head, rule.body
        r = None
        if rule_type == RuleTypes.HEAD:
            r = list(head)
        elif rule_type == RuleTypes.BODY:
            r = list(body)
        else:
            r = list(head) + list(body)
        if isinstance(rule_count, (int, float)):
            c = int(rule_count)
            if len(r) == c:
                relevant_rules.add(rule)
    return list(relevant_rules), len(relevant_rules)

def lookup_join(rules, op, *args):
    result = []
    one, two, mixed = False, False, False
    arguments = None
    args1, args2 = None, None
    if op in (TemplateOperations.ONEANDONE, TemplateOperations.ONEORONE):
        args1, args2 = (args[0:3]), (args[3:])
        one = True
    elif op in (TemplateOperations.TWOANDTWO, TemplateOperations.TWOORTWO):
        args1, args2 = (args[0:2]), (args[2:])
        two = True
    elif op in (TemplateOperations.ONEORTWO, TemplateOperations.ONEANDTWO):
        args1, args2 = (args[0:3]), (args[3:])
        mixed = True
    if one:
        r1, c1 = lookup(rules, *args1)
        r2, c2 = lookup(rules, *args2)
        r3 = r1 + r2
        if TemplateOperations.ONEANDONE:
            result = [r for r in r3 if r in r1 and r in r2]
        else:
            result = r3
    if two:
        r1, c1 = lookup_size(rules, *args1)
        r2, c2 = lookup_size(rules, *args2)
        r3 = r1 + r2
        if TemplateOperations.TWOANDTWO:
            result = [r for r in r3 if r in r1 and r in r2]
        else:
            result = r3

    if mixed:
        r1, c1 = lookup(rules, *args1)
        r2, c2 = lookup_size(rules, *args2)
        r3 = r1 + r2
        if TemplateOperations.ONEANDTWO:
            result = [r for r in r3 if r in r1 and r in r2]
        else:
            result = r3
    return list(set(result)), len(set(result))

def main():
    args = setup_argparser().parse_args()
    filename = args.file
    confidence = args.confidence
    support = args.support
    lines = load_data(filename)
    genes, diseases = format_data(lines)
    item_len_counts = collections.defaultdict(int)
    print("Support: {0}, Confidence: {1}".format(support, confidence))
    items, rules = compute_apriori(genes, min_support=support, min_confidence=confidence)
    for item, support in items:
        item_len_counts[len(item)] += 1
        print("Item: {}, Support: {}".format(item, support))
    print("Frequent item set length counts {}".format(item_len_counts))
    for rule in rules:
        print(rule)

    # T1 Queries
    # print("Template 1 Queries")
    # r, c = lookup(rules, RuleTypes.RULE, RuleCounts.ANY, ['G59_Up'])
    # print("Count {0}".format(c))
    # r, c = lookup(rules, RuleTypes.RULE, RuleCounts.NONE, ['G59_Up'])
    # print("Count {0}".format(c))
    # r, c = lookup(rules, RuleTypes.RULE, 1, ['G59_Up', 'G10_Down'])
    # print("Count {0}".format(c))
    # r, c = lookup(rules, RuleTypes.HEAD, RuleCounts.ANY, ['G59_Up'])
    # print("Count {0}".format(c))
    # r, c = lookup(rules, RuleTypes.HEAD, RuleCounts.NONE, ['G59_Up'])
    # print("Count {0}".format(c))
    # r, c = lookup(rules, RuleTypes.HEAD, 1, ['G59_Up', 'G10_Down'])
    # print("Count {0}".format(c))
    # r, c = lookup(rules, RuleTypes.BODY, RuleCounts.ANY, ['G59_Up'])
    # print("Count {0}".format(c))
    # r, c = lookup(rules, RuleTypes.BODY, RuleCounts.NONE, ['G59_Up'])
    # print("Count {0}".format(c))
    # r, c = lookup(rules, RuleTypes.BODY, 1, ['G59_Up', 'G10_Down'])
    # print("Count {0}".format(c))

    # # T2 Queries
    # print("Template 2 Queries")
    # r, c = lookup_size(rules, RuleTypes.RULE, 3)
    # print("Count {0}".format(c))
    # r, c = lookup_size(rules, RuleTypes.HEAD, 2)
    # print("Count {0}".format(c))
    # r, c = lookup_size(rules, RuleTypes.BODY, 1)
    # print("Count {0}".format(c))

    # # T3 Queries
    # print("Template 3 Queries")
    # r, c = lookup_join(rules, TemplateOperations.ONEORONE, RuleTypes.HEAD, RuleCounts.ANY, ['G10_Down'], RuleTypes.BODY, 1, ['G59_Up'])
    # print("Count {0}".format(c))
    # r, c = lookup_join(rules, TemplateOperations.ONEANDONE, RuleTypes.HEAD, RuleCounts.ANY, ['G10_Down'], RuleTypes.BODY, 1, ['G59_Up'])
    # print("Count {0}".format(c))
    # r, c = lookup_join(rules, TemplateOperations.ONEORTWO, RuleTypes.HEAD, RuleCounts.ANY, ['G10_Down'], RuleTypes.BODY, 2)
    # print("Count {0}".format(c))
    # r, c = lookup_join(rules, TemplateOperations.ONEANDTWO, RuleTypes.HEAD, RuleCounts.ANY, ['G10_Down'], RuleTypes.BODY, 2)
    # print("Count {0}".format(c))
    # r, c = lookup_join(rules, TemplateOperations.TWOORTWO, RuleTypes.HEAD, 1, RuleTypes.BODY, 2)
    # print("Count {0}".format(c))
    # r, c = lookup_join(rules, TemplateOperations.TWOANDTWO, RuleTypes.HEAD, 1, RuleTypes.BODY, 2)
    # print("Count {0}".format(c))

    # print("Demo query")
    # r, c = lookup_join(rules, TemplateOperations.ONEORTWO, RuleTypes.BODY, RuleCounts.ANY, ['G1_Down'], RuleTypes.HEAD, 2)
    # print("Results: {0}, Count: {1}".format(r, c))

    r, c = lookup(rules, RuleTypes.HEAD, RuleCounts.NONE, ['G1_Up'])
    print(c)
    r, c = lookup(rules, RuleTypes.HEAD, 1, ['G1_Up', 'G10_Down'])
    print(c)
    r, c = lookup_size(rules, RuleTypes.RULE, 2)
    print(c)
    r, c = lookup_size(rules, RuleTypes.BODY, 2)
    print(c)
    r, c = lookup_join(rules, TemplateOperations.ONEORTWO, RuleTypes.HEAD, RuleCounts.ANY, ['G1_Up'], RuleTypes.BODY, 2)
    print(c)
    r, c = lookup_join(rules, TemplateOperations.ONEANDTWO, RuleTypes.HEAD, RuleCounts.ANY, ['G1_Up'], RuleTypes.BODY, 2)
    print(c)
    print(lookup_join(rules, TemplateOperations.TWOANDTWO, RuleTypes.HEAD, 1, RuleTypes.BODY, 2))
    return

if __name__ == "__main__":
    main()
