## How to run
In order to run this program, we need Python 3.

To run this program on a particular text file, execute the command `python3 apriori.py -f <file> --support <support> --confidence <confidence>`. It will display a list of frequent item sets, and associated rules.

To see the results of particular queries, they are currently commented out in the `main()` function, with each template query type as its own block of code, i.e. Template 1 queries are under `# T1 Queries`, Template 2 under `# T2 Queries`, and Template 3 under `# T3 Queries`.
